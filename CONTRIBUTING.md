<div align="center">
  <h1> 📚👤 Guide des bonnes pratiques de contribution au projet</h1>
</div>
<br/>

Cette page présente les bonnes pratiques pour la contribution au projet.

## Contribution

- Vu que l'on ne souhaite pas que certains processus soient déployés en production, nous travaillons avec une branche pour REC (Recette) et une branche pour PROD (Production).
- Créez une branche d'évolution ou de correction à partir de la branche **ctdevelop**.
- Pour un correctif urgent, créez une branche dédiée à partir de la dernière release (**last release** ou **master**).
> [!IMPORTANT]   Si une évolution ou correction ne doit pas être livrée dans la prochaine version de l'application, elle ne doit pas être intégrée à la branche release qui part en PROD.

### Processus de Livraison en REC (*rec1,rec6,...*)

- Créez une branche à partir de ctdevelop, apportez vos modifications et livrez sur votre environnement sans merger avec ctdevelop.
- Personne ne peut pousser directement sur la branche ctdevelop.
- Après livraison en prod, soumettez une `pull request` pour merger avec ctdevelop.
- Chaque tribu a une tag spécifique sous le format : tribSquad.x.x, et une demande de changement pour livrer en REC (*exemple tag* : ***projet Hadel*** : **31.x.x**).


### Processus de Livraison en PROD

- Créez une branche release (« current release ») à partir de la dernière release (ou de la branche master).
- Chaque équipe crée une nouvelle branche pour y apporter ses modifications destinées à la livraison en PROD.
- Une fois les modifications effectuées, une pull request est soumise pour fusionner avec la branche current release. Les équipes respectives chargées de ces branches valident les pull requests, vérifiant particulièrement l'absence de régressions, surtout au niveau de la BDM.
- Après installation en environnement de PROD, une personne effectue le push de la release sur la branche MASTER. (la branche : current release devient la branche last release).

> [!IMPORTANT] Pour BDM, il est `nécessaire` d'ajouter manuellement les modifications que l'on souhaite intégrer en PROD. Il ne faut pas exporter et importer la BDM ni fusionner avec la branche release, afin d'éviter que des tables ou des champs spécifiques à certains processus existant dans la branche de développement ne se retrouvent en production.


![gitflow-prod](doc/schemas/Livraison-PROD.svg)

### Revues de Code
![review](doc/schemas/review.svg)

### Pull Request

- Une PR est revue par au moins 1 des dev, et dans le cas de livraison en PROD, la revue sera effectuée par un membre de l'autre équipe.
- Dans la mesure du possible, ajoutez un descriptif complet dans la PR pour faciliter la revue de code.
- Avant de soumettre la PR, dans le cas où d'autres développements ont été intégrés à la branche de dev, faites un pull de cette branche.

### Conventions de Nommage
| Catégories       | Conventions                                                                                                       |
| --------------- | ------------------------------------------------------------------------------------------------------------------ |
| **Branches**    | - 🌱  `feature/CASEMGT-année-sprint.version/BPM-xxx` : pour une évolution.                             |
|                 | - 🔧 ` fix/CASEMGT-année-sprint.version/BPM-xxx` : pour les correctifs.                                |
|                 | - 📚 ` quality/xxx` : pour la documentation, amélioration du code, etc.                                              |
|                 | - 🚀 ` release/CASEMGT-année-sprint.version` : pour les releases à livrer en PROD (exemple : `release/CASEMGT-2024-2.01`). |
||                                                                               
| **Pull Requests**| -  Doit être préfixé par le numéro de Jira. |



## TAG RELEASE

- **Pour les releases de recette** : ce tag doit avoir le pattern suivant : `tribSquad.x.x` (exemple : `trib 3, squad 1 => 31.0.405`).
- **Pour le PROD** : un tag Git doit être posé afin de pouvoir facilement faire le lien entre la version GESLIV et l'historique Git. Ce tag doit avoir le pattern suivant : `5.x.x`.

```vue
EL HAYANI Hassan
```
