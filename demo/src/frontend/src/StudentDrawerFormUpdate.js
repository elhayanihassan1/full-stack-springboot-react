/*import {Drawer, Input, Col, Select, Form, Row, Button, Spin} from 'antd';
import{useState} from 'react'
import {LoadingOutlined} from "@ant-design/icons";
import { successNotification, errorNotification} from "./Notification";
import {getStudentById, updateStudent} from "./client";


const {Option} = Select;
const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;




function StudentDrawerFormUpdate({showDrawerUpdate, setShowDrawerUpdate,fetchStudent}) {

    const onCLose = () => setShowDrawerUpdate(false);
    const[submittingUpdate, setSubmittingUpdate] = useState(false);

    const onFinish = (studentId,student) => {

        setSubmittingUpdate(true);

        updateStudent(studentId,student).then(() => {

                console.log("student updated")
                onCLose();
                successNotification("Student successfully updated",
                    `${student.name} was updated in the system`);
                fetchStudent();
            })
            .catch( err => {
            console.log(err.response);
            err.response.json().then(res => {
                console.log(res);
                errorNotification(
                    "There was an issue",
                    `${res.message} [${res.status}] [${res.error}]`,
                    "bottomLeft"
                   )
               });
            })
            .finally(()=> {
            setSubmittingUpdate(false);
        })
    };

    const onFinishFailed = errorInfo => {
        alert(JSON.stringify(errorInfo, null, 2));
    };

    return <Drawer
        title="update  student"
        width={720}
        onClose={onCLose}
        visible={showDrawerUpdate}
        bodyStyle={{paddingBottom: 80}}
        footer={
            <div
                style={{
                    textAlign: 'right',
                }}
            >
                <Button onClick={onCLose} style={{marginRight: 8}}>
                    Cancel
                </Button>
            </div>
        }
    >
        <Form layout="vertical"
              onFinishFailed={onFinishFailed}
              onFinish={onFinish}
              hideRequiredMark>
            <Row gutter={16}>
                <Col span={12}>
                    <Form.Item
                        name="name"
                        label="Name"
                        rules={[{required: true, message: 'Please enter student name'}]}
                    >
                        <Input placeholder="Please enter student email" />
                    </Form.Item>
                </Col>
                <Col span={12}>
                    <Form.Item
                        name="email"
                        label="Email"
                        rules={[{required: true, message: 'Please enter student email'}]}
                    >
                        <Input placeholder="Please enter student email"/>
                    </Form.Item>
                </Col>
            </Row>
            <Row gutter={16}>
                <Col span={12}>
                    <Form.Item
                        name="gender"
                        label="gender"
                        rules={[{required: true, message: 'Please select a gender'}]}
                    >
                        <Select placeholder="Please select a gender">
                            <Option value="MALE">MALE</Option>
                            <Option value="FEMALE">FEMALE</Option>
                            <Option value="OTHER">OTHER</Option>
                        </Select>
                    </Form.Item>
                </Col>
            </Row>
            <Row>
                <Col span={12}>
                    <Form.Item >
                        <Button type="primary" htmlType="submit">
                            Update
                        </Button>
                    </Form.Item>
                </Col>
            </Row>
            <Row>
                {submittingUpdate && <Spin indicator={antIcon} />}
            </Row>
        </Form>
    </Drawer>
}

export default StudentDrawerFormUpdate;  */