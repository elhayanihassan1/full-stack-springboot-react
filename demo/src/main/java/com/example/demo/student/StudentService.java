package com.example.demo.student;

import com.example.demo.student.exception.BadRequestException;
import com.example.demo.student.exception.StudentNotFoundException;
import lombok.AllArgsConstructor;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.springframework.boot.context.config.ConfigDataResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@AllArgsConstructor
@Service
public class StudentService {

    private final StudentRepository studentRepository;

    public List<Student> getAllStudents() {

        return studentRepository.findAll();
    }

    public void addStudent(Student student) {
        Boolean existsEmail = studentRepository.selectExistsEmail(student.getEmail());
        if (existsEmail) {
            throw new BadRequestException(
                    "Email " + student.getEmail() + " taken");
        }
        studentRepository.save(student);
    }

    public void deleteStudent(Long studentId) {
        if (!studentRepository.existsById(studentId)) {
            throw new StudentNotFoundException(
                    "Student with id " + studentId + " does not exists");
        }
        studentRepository.deleteById(studentId);
    }


    public ResponseEntity<Student> getStudentById(Long studentId) {

      Student student=  studentRepository.findById(studentId).orElseThrow(() -> new ResourceNotFoundException(" Student not exist with id: " +studentId));
      return ResponseEntity.ok(student);
    }

    public ResponseEntity<Student> updateStudent(Long studentId , Student studentDetails){

        Student student = studentRepository.findById(studentId).orElseThrow(() -> new ResourceNotFoundException(" Student not exist with id: " +studentId));
        student.setName(studentDetails.getName());
        student.setEmail(studentDetails.getEmail());
        student.setGender(studentDetails.getGender());
        Student updateStudent = studentRepository.save(student);
        return ResponseEntity.ok(updateStudent);
    }

}
