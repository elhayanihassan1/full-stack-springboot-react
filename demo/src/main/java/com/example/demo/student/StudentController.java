package com.example.demo.student;


import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path= "api/v1/students")
@AllArgsConstructor
public class StudentController {


    private final StudentService  studentService;


    @GetMapping
    public List<Student> getAllStudents(){
              return studentService.getAllStudents();
    }

    @PostMapping
    public void addStudent(@Valid @RequestBody Student student) {
              studentService.addStudent(student);
    }

    @DeleteMapping("{studentId}")
    public void deleteStudent(@PathVariable("studentId") Long studentId){
        studentService.deleteStudent(studentId);
    }
/*
    @PutMapping("{studentId}")
    public ResponseEntity<Student> updateStudent(@PathVariable Long studentId , @RequestBody Student student){
        return studentService.updateStudent(studentId,student);
    } */

    @GetMapping("{studentId}")
    public ResponseEntity<Student> getStudentById(@PathVariable Long studentId){
         return  studentService.getStudentById(studentId);
    }

}
